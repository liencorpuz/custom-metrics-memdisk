#!/bin/bash

line="*/5 * * * * ~/custom-metrics-memdisk/aws-scripts-mon/mon-put-instance-data.pl --mem-used --mem-used-incl-cache-buff --mem-util --mem-avail --disk-space-used --disk-space-util --disk-space-avail --disk-path=/ --from-cron"
(crontab -u root -l; echo "$line" ) | crontab -u root -

#*/5 * * * * ~/custom-metrics-memdisk/aws-scripts-mon/mon-put-instance-data.pl --mem-used --mem-used-incl-cache-buff --mem-util --mem-avail --disk-space-used --disk-space-util --disk-space-avail --disk-path=/ --from-cron