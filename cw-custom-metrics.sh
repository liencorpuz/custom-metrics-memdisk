#!/bin/bash

echo "===================================================="
echo "GLOBE DSG INFRASTRUCTURE"
echo "WELCOME! DSG INSTALLATION OF SEC-AGENTS"
echo "==================================================="
echo

#INSTANCE_IDINFO=$(cat ec2instanceid.txt)

if [ -f /etc/os-release ]; then
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
    ID_OS=$ID
    OS_VER="${VER:0:2}"
    OS_VER_AMZ="${VER:0:4}"
    echo $NAME "$OS_VER"

    if [ $ID == "centos" ] && [ $OS_VER -eq "7" ]
    then
        #Download and Install required package
        sudo yum install -y unzip
        sudo yum install -y perl-Switch perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-https perl-Digest-SHA.x86_64
        #Download and install and run CW Scripts
        curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O
        sudo unzip CloudWatchMonitoringScripts-1.2.2.zip && \
        sudo rm -rf CloudWatchMonitoringScripts-1.2.2.zip && \
        cd aws-scripts-mon
        ./mon-put-instance-data.pl --mem-used --mem-util --mem-used-incl-cache-buff --mem-avail --mem-used --disk-space-used --disk-space-util --disk-path=/ --disk-space-avail

    fi

    if [ $ID == "ubuntu" ] && [ $OS_VER == "16" ]
    then
        #Download and Install required package
        sudo apt-get update
        sudo apt-get install unzip -y
        sudo apt-get install libwww-perl libdatetime-perl
        #Download and install and run CW Scripts
        curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O
        sudo unzip CloudWatchMonitoringScripts-1.2.2.zip && \
        sudo rm -rf CloudWatchMonitoringScripts-1.2.2.zip && \
        cd aws-scripts-mon
        ./mon-put-instance-data.pl --mem-used --mem-util --mem-used-incl-cache-buff --mem-avail --mem-used --disk-space-used --disk-space-util --disk-path=/ --disk-space-avail

    fi

       if [ $ID == "ubuntu" ] && [ $OS_VER == "18" ]
    then

        #Download and Install required package
        sudo apt-get update
        sudo apt-get install unzip -y
        sudo apt-get install libwww-perl libdatetime-perl -y
        #Download and install and run CW Scripts
        curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O
        sudo unzip CloudWatchMonitoringScripts-1.2.2.zip && \
        sudo rm -rf CloudWatchMonitoringScripts-1.2.2.zip && \
        cd aws-scripts-mon
        ./mon-put-instance-data.pl --mem-used --mem-util --mem-used-incl-cache-buff --mem-avail --mem-used --disk-space-used --disk-space-util --disk-path=/ --disk-space-avail

    fi

           if [ $ID == "ubuntu" ] && [ $OS_VER == "20" ]
    then
        #Download and Install required package
        sudo apt-get update
        sudo apt-get install unzip -y
        sudo apt-get install libwww-perl libdatetime-perl -y
        #Download and install and run CW Scripts
        curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O
        sudo unzip CloudWatchMonitoringScripts-1.2.2.zip && \
        sudo rm -rf CloudWatchMonitoringScripts-1.2.2.zip && \
        cd aws-scripts-mon
        ./mon-put-instance-data.pl --mem-used --mem-util --mem-used-incl-cache-buff --mem-avail --mem-used --disk-space-used --disk-space-util --disk-path=/ --disk-space-avail

        

    fi


    if [ $ID == "ubuntu" ] && [ $OS_VER == "14" ]
    then
        #Download and Install required package
        sudo apt-get update 
        sudo apt-get install unzip -y
        sudo apt-get install libwww-perl libdatetime-perl -y
        #Download and install and run CW Scripts
        curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O
        sudo unzip CloudWatchMonitoringScripts-1.2.2.zip && \
        sudo rm -rf CloudWatchMonitoringScripts-1.2.2.zip && \
        cd aws-scripts-mon
        ./mon-put-instance-data.pl --mem-used --mem-util --mem-used-incl-cache-buff --mem-avail --mem-used --disk-space-used --disk-space-util --disk-path=/ --disk-space-avail


    fi


    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2018" ]
    then
        #Download and Install required package
        sudo yum install -y unzip
        sudo yum install -y perl-Switch perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-https perl-Digest-SHA.x86_64
        #Download and install and run CW Scripts
        curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O
        sudo unzip CloudWatchMonitoringScripts-1.2.2.zip && \
        sudo rm -rf CloudWatchMonitoringScripts-1.2.2.zip && \
        cd aws-scripts-mon
        ./mon-put-instance-data.pl --mem-used --mem-util --mem-used-incl-cache-buff --mem-avail --mem-used --disk-space-used --disk-space-util --disk-path=/ --disk-space-avail


    fi

    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2015" ]
    then        
        #Download and Install required package
        sudo yum install -y unzip
        sudo yum install -y perl-Switch perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-https perl-Digest-SHA.x86_64
        #Download and install and run CW Scripts
        curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O
        sudo unzip CloudWatchMonitoringScripts-1.2.2.zip && \
        sudo rm -rf CloudWatchMonitoringScripts-1.2.2.zip && \
        cd aws-scripts-mon
        ./mon-put-instance-data.pl --mem-used --mem-util --mem-used-incl-cache-buff --mem-avail --mem-used --disk-space-used --disk-space-util --disk-path=/ --disk-space-avail

        


    fi

    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2017" ]
    then
        #Download and Install required package
        sudo yum install -y unzip
        sudo yum install -y perl-Switch perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-https perl-Digest-SHA.x86_64
        #Download and install and run CW Scripts
        curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O
        sudo unzip CloudWatchMonitoringScripts-1.2.2.zip && \
        sudo rm -rf CloudWatchMonitoringScripts-1.2.2.zip && \
        cd aws-scripts-mon
        ./mon-put-instance-data.pl --mem-used --mem-util --mem-used-incl-cache-buff --mem-avail --mem-used --disk-space-used --disk-space-util --disk-path=/ --disk-space-avail

    fi

    if [ $ID == "amzn" ] && [ $OS_VER == "2" ]
    then
        #Download and Install required package
        sudo yum install -y unzip
        sudo yum install -y perl-Switch perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-https perl-Digest-SHA.x86_64
        #Download and install and run CW Scripts
        curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O
        sudo unzip CloudWatchMonitoringScripts-1.2.2.zip && \
        sudo rm -rf CloudWatchMonitoringScripts-1.2.2.zip && \
        cd aws-scripts-mon
        ./mon-put-instance-data.pl --mem-used --mem-util --mem-used-incl-cache-buff --mem-avail --mem-used --disk-space-used --disk-space-util --disk-path=/ --disk-space-avail


    fi




    if [ $ID == "rhel" ] && [ $OS_VER == "7" ]
    then
        #Download and Install required package
        sudo yum install -y unzip
        sudo yum install perl-Switch perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-httpsperl-Digest-SHA --enablerepo="rhui-REGION-rhel-server-optional" -y
        sudo yum install perl-Switch perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-https perl-Digest-SHA --enablerepo="rhui-REGION-rhel-server-optional" -y
        sudo yum install zip unzip
        #Download and install and run CW Scripts
        curl https://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.2.zip -O
        sudo unzip CloudWatchMonitoringScripts-1.2.2.zip && \
        sudo rm -rf CloudWatchMonitoringScripts-1.2.2.zip && \
        cd aws-scripts-mon
        ./mon-put-instance-data.pl --mem-used --mem-util --mem-used-incl-cache-buff --mem-avail --mem-used --disk-space-used --disk-space-util --disk-path=/ --disk-space-avail


        
    fi

fi

    echo
    echo "Completed..."

 
